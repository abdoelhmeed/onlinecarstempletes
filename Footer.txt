 <footer class="footer-container typefooter-4">
            <!-- Footer Top Container -->
            <div class="row-top">
                <div class="block-services container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-margin1">
                            <div class="icon-service">
                                <div class="icon"><i class="pe-7s-car">&nbsp;</i></div>
                                <div class="text">
                                    <h6>Free shipping</h6>
                                    <p class="no-margin">On all orders over $99.00</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-margin1">
                            <div class="icon-service">
                                <div class="icon"><i class="pe-7s-refresh-2">&nbsp;</i></div>
                                <div class="text">
                                    <h6>30 days return</h6>
                                    <p class="no-margin">You have 30 days to return</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-margin">
                            <div class="icon-service">
                                <div class="icon"><i class="pe-7s-door-lock">&nbsp;</i></div>
                                <div class="text">
                                    <h6>Safe Shopping<br></h6>
                                    <p class="no-margin">Payment 100% secure</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="icon-service">
                                <div class="icon"><i class="pe-7s-users">&nbsp;</i></div>
                                <div class="text">
                                    <h6>Online support</h6>
                                    <p class="no-margin">Contact us 24 hours a day</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /Footer Top Container -->
            <!-- Footer middle Container -->
            <div class="container">
                <div class="row footer-middle">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-style">
                        <div class="box-footer box-infos">
                            <div class="module">
                                <h3 class="modtitle">Contact us</h3>
                                <div class="modcontent">
                                    <ul class="list-icon">
                                        <li><span class="icon pe-7s-map-marker"></span> VA 20155</li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-style">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-style">
                                <div class="box-information box-footer">
                                    <div class="module clearfix">
                                        <h3 class="modtitle">Information</h3>
                                        <div class="modcontent">
                                            <ul class="menu">
                                                <li><a href="#">About Us</a></li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-style">
                                <div class="box-account box-footer">
                                    <div class="module clearfix">
                                        <h3 class="modtitle">My Account</h3>
                                        <div class="modcontent">
                                            <ul class="menu">
                                                <li><a href="#">Brands</a></li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-clear">
                                <div class="box-service box-footer">
                                    <div class="module clearfix">
                                        <h3 class="modtitle">Categories</h3>
                                        <div class="modcontent">
                                            <ul class="menu">
                                                <li><a href="#">Event & Party Supplies</a></li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="jumbotron" style="background-color: #109EDA; color: white;">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <h3>Notity Me</h3>
                            <p style="font-size: 10px;">Set your Alerts for Mitsubishi Ek Wagon in Karachi and we will
                                email you relevant ads.</p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"> <input type="text" class="form-control"></div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><button
                                class="btn btn-block btn-warning">Submit</button></div>
                    </div>
                </div>
            </div>
            <!-- /Footer middle Container -->
            <!-- Footer Bottom Container -->
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="copyright col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <p>Autoparts © 2019 Demo Store. All Rights Reserved. Designed by <a href="#"
                                    target="_blank">OpenCartWorks.Com</a></p>
                        </div>
                        <div class="payment-w col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <img src="image/catalog/demo/payment/payment.png" alt="imgpayment">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Footer Bottom Container -->
            <!--Back To Top-->
            <div class="back-to-top"><i class="fa fa-angle-up"></i></div>
        </footer>


